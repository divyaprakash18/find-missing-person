package com.findmissingperson.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.findmissingperson.model.ContactPerson;
import com.findmissingperson.model.LostPerson;
import com.findmissingperson.model.User;
import com.findmissingperson.service.ContactPersonService;
import com.findmissingperson.service.LostPersonService;
import com.findmissingperson.service.UserService;

@Controller
public class UserDashBoardController {

	@Autowired
	private LostPersonService lostPersonService;

	@Autowired
	private UserService userService;

	@Autowired
	private ContactPersonService contactPersonService;

	@RequestMapping(value = "/reportMissingForm", method = RequestMethod.GET)
	public String showReportMissingForm(Model model) {
		/* model.addAttribute("message", ""); */
		return "reportMissingForm";
	}

	@RequestMapping(value = "/foundSomeone", method = RequestMethod.GET)
	public String foundSomeonePage(Model model) {
		/* model.addAttribute("message", ""); */
		return "foundSomeone";
	}

	// controller for showing registration form
	@RequestMapping(value = "/showUserForm", method = RequestMethod.GET)
	public String addUser() {
		System.out.println("inside controller..");
		return "registerUser";
	}

	// Controller for adding Missing Person
	@RequestMapping(value = "/addMissing", method = RequestMethod.POST)
	public String addMissingPerson(@ModelAttribute("lostPerson") LostPerson lostPerson) {
		System.out.println("inside controller..");
		this.lostPersonService.addLostPerson(lostPerson);
		return "successful";
	}

	// Controller for adding user
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User user) {
		System.out.println("inside controller..");
		this.userService.addUser(user);
		return "successful";
	}

	

	// Controller for adding user              //lost someone login form should open from here
	@RequestMapping(value = "/addContactPerson", method = RequestMethod.POST)
	public String addContactPerson(@ModelAttribute("contactPerson") ContactPerson contactPerson) {
		System.out.println("inside controller..");
		this.contactPersonService.addContactPerson(contactPerson);
		
		return "lostSomeoneLogin";
	}

	@RequestMapping(value = "/addHelper", method = RequestMethod.POST)
	public String addHelper(@ModelAttribute("user") User user) {
		System.out.println("inside add helper controller..");
		/*addMissingPerson(@ModelAttribute("lostPerson") LostPerson lostPerson) {
		System.out.println("inside controller..");
		this.lostPersonService.addLostPerson(lostPerson)*/
		
		this.userService.addUser(user);
		
		return "foundSomeoneLogin";
	}
	// controller for opening search page
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String search() {
		System.out.println("inside controller..");
		return "search";
	}


	// Controller for adding user
		@RequestMapping(value = "/addRelative", method = RequestMethod.POST)
		public String addLostPerson(@ModelAttribute("user") User user) {
			System.out.println("inside controller..");
			this.userService.addUser(user);
			return "addRelative";
		}
}
