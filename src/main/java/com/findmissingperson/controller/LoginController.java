package com.findmissingperson.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.findmissingperson.model.ContactPerson;
import com.findmissingperson.model.User;
import com.findmissingperson.service.ContactPersonService;
import com.findmissingperson.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private ContactPersonService contactPersonService;

	public void setContactPersonService(ContactPersonService contactPersonService) {
		this.contactPersonService = contactPersonService;
	}
	
	@Autowired
	private UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	// Login Controller for User
	@RequestMapping(value = "/userDashboard1", method = RequestMethod.POST)
	public String showUserDashboard(@RequestParam("phone") String phone, @RequestParam("password") String password,
			Model model) {
		ContactPerson contactPerson = null;
		contactPerson = this.contactPersonService.getContactPersonByPhone(phone);
		System.out.println(contactPerson);
		if (null != contactPerson) {
			if (null != contactPerson.getPassword())
				if (contactPerson.getPassword().equals(password))
					return "reportMissingForm";
		}
		return "redirect:/lostSomeoneLogin";
	}
	
	@RequestMapping(value = "/userDashboard2", method = RequestMethod.POST)
	public String showUsersDashboard(@RequestParam("phone") String phone, @RequestParam("password") String password,
			Model model) {
		User user = null;
		user = this.userService.getUserByPhone(phone);
		System.out.println(user);
		if (null != user) {
			if (null != user.getPassword())
				if (user.getPassword().equals(password))
					return "successful";
		}
		return "redirect:/foundSomeoneLogin";
	}
}
