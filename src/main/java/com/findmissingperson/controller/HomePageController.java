package com.findmissingperson.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.findmissingperson.model.LostPerson;

@Controller
public class HomePageController {

	// object declaration of any service to be used

	/*
	 * @Autowired public void setEmployeeService(EmployeeService employeeService) {
	 * this.employeeService = employeeService; }
	 */

	@RequestMapping(value = "/first", method = RequestMethod.GET)
	public String firstPage(Model model) {
		/* model.addAttribute("message", ""); */
		return "homepage";
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.GET)
	public String registerUser(Model model) {/*
												 * model.addAttribute("message", "user");
												 */
		return "registerUser";
	}

	// controller for showing registration form
	@RequestMapping(value = "/showContactPersonForm", method = RequestMethod.GET)
	public String addContactPerson() {
		System.out.println("inside controller..");
		return "registerContactPerson";
	}

	@RequestMapping(value = "/lostSomeoneLogin", method = RequestMethod.GET)
	public String showLoginFormForLostSomeone() {
		System.out.println("inside controller..");
		return "lostSomeoneLogin";
	}

	@RequestMapping(value = "/foundSomeoneLogin", method = RequestMethod.GET)
	public String showLoginFormForFoundSomeone() {
		System.out.println("inside controller..");
		return "foundSomeoneLogin";
	}

	// controller for search

}
