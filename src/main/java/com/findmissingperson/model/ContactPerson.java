package com.findmissingperson.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ContactPerson {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int contactPersonId;

	private String fName;

	private String lName;

	private String email;
	private String phone;
	private String address1;
	private String address2;
	private String pincode;
	private String password;
	private String relationWithLost;

	@Override
	public String toString() {
		return "ContactPerson [contactPersonId=" + contactPersonId + ", fName=" + fName + ", lName=" + lName
				+ ", email=" + email + ", phone=" + phone + ", address1=" + address1 + ", address2=" + address2
				+ ", pincode=" + pincode + ", password=" + password + ", relationWithLost=" + relationWithLost + "]";
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public int getContactPersonId() {
		return contactPersonId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRelationWithLost() {
		return relationWithLost;
	}

	public void setRelationWithLost(String relationWithLost) {
		this.relationWithLost = relationWithLost;
	}
}
