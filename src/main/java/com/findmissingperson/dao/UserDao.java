package com.findmissingperson.dao;

import com.findmissingperson.model.User;

public interface UserDao {
	
	public void addUser(User user);
	
	public User getUserByPhone(String phone);

}
