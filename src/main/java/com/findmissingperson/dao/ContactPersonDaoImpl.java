package com.findmissingperson.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.findmissingperson.model.ContactPerson;

@Repository
public class ContactPersonDaoImpl implements ContactPersonDao {

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public void addContactPerson(ContactPerson contactPerson) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(contactPerson);

	}

	@Override
	@Transactional
	public ContactPerson getContactPersonByPhone(String phone) {
		ContactPerson c = null;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM ContactPerson c WHERE c.phone= :phone");
			query.setParameter("phone", phone);
			List<ContactPerson> results = ((org.hibernate.query.Query) query).list();
			c = results.get(0);

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(c);
		return c;
	}

}
