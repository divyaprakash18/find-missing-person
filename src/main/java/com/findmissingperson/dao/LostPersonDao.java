package com.findmissingperson.dao;

import com.findmissingperson.model.LostPerson;

public interface LostPersonDao {
	
	public void addLostPerson(LostPerson lostPerson);
	
	public void removeLostPerson(int id);

}
