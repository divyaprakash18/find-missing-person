package com.findmissingperson.dao;

import com.findmissingperson.model.ContactPerson;

public interface ContactPersonDao {
	
	public void addContactPerson(ContactPerson contactPerson);

	public ContactPerson getContactPersonByPhone(String phone);
}
