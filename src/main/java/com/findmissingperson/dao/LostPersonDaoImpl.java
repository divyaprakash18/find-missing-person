package com.findmissingperson.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.findmissingperson.model.LostPerson;

@Repository
public class LostPersonDaoImpl implements LostPersonDao {

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public void addLostPerson(LostPerson lostPerson) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(lostPerson);
	}

	@Override
	@Transactional
	public void removeLostPerson(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		LostPerson lp = (LostPerson) session.load(LostPerson.class, new Integer(id));
		if (null != lp) {
			session.delete(lp);
		}

	}

}
