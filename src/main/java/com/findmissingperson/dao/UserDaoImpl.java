package com.findmissingperson.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.findmissingperson.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	// Implemented

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public void addUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(user);
	}

	@Override
	@Transactional
	public User getUserByPhone(String phone) {
		// TODO Auto-generated method stub
		User user = null;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM User user WHERE user.phone= :phone");
			query.setParameter("phone", phone);
			List<User> results = ((org.hibernate.query.Query) query).list();
			user = results.get(0);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
}
