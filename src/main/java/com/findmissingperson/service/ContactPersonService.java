package com.findmissingperson.service;

import com.findmissingperson.model.ContactPerson;

public interface ContactPersonService {

	public void addContactPerson(ContactPerson contactPerson);

	public ContactPerson getContactPersonByPhone(String phone);

}