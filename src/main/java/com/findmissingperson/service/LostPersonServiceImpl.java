package com.findmissingperson.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findmissingperson.dao.LostPersonDao;
import com.findmissingperson.model.LostPerson;

@Service
public class LostPersonServiceImpl implements LostPersonService {

	@Autowired
	private LostPersonDao lostPersonDao;

	public LostPersonDao getLostPersonDao() {
		return lostPersonDao;
	}

	public void setLostPersonDao(LostPersonDao lostPersonDao) {
		this.lostPersonDao = lostPersonDao;
	}

	@Override
	public void addLostPerson(LostPerson lostPerson) {
		// TODO Auto-generated method stub
		this.lostPersonDao.addLostPerson(lostPerson);
	}

	@Override
	public void removeLostPerson(int id) {
		// TODO Auto-generated method stub
		this.lostPersonDao.removeLostPerson(id);
	}

}
