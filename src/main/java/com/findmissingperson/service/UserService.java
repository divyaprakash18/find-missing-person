package com.findmissingperson.service;

import com.findmissingperson.model.User;

public interface UserService {

	public void addUser(User user);

	public User getUserByPhone(String phone);

}
