package com.findmissingperson.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findmissingperson.dao.UserDao;
import com.findmissingperson.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
		this.userDao.addUser(user);
	}

	@Override
	public User getUserByPhone(String phone) {
		// TODO Auto-generated method stub
		User user = this.userDao.getUserByPhone(phone);
		return user;
	}

}
