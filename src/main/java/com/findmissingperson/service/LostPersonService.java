package com.findmissingperson.service;

import com.findmissingperson.model.LostPerson;

public interface LostPersonService {

	public void addLostPerson(LostPerson lostPerson);

	public void removeLostPerson(int id);

}
