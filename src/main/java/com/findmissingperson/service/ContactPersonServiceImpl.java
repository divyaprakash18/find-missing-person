package com.findmissingperson.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.findmissingperson.dao.ContactPersonDao;
import com.findmissingperson.model.ContactPerson;

@Service
public class ContactPersonServiceImpl implements ContactPersonService {

	@Autowired
	private ContactPersonDao contactPersonDao;

	public void setContactPersonDao(ContactPersonDao contactPersonDao) {
		this.contactPersonDao = contactPersonDao;
	}

	public ContactPersonDao getContactPersonDao() {
		return contactPersonDao;
	}

	@Override
	public void addContactPerson(ContactPerson contactPerson) {
		// TODO Auto-generated method stub
		this.contactPersonDao.addContactPerson(contactPerson);
	}

	@Override
	public ContactPerson getContactPersonByPhone(String phone) {
		// TODO Auto-generated method stub
		ContactPerson contactPerson = this.contactPersonDao.getContactPersonByPhone(phone);
		return contactPerson;
	}

}