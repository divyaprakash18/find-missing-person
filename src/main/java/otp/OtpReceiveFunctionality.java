package otp;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import java.util.*;
public class OtpReceiveFunctionality {
	
	private static String session_id;
	private static String otp_entered_by_user;
	private static String api_key="44c3cfe8-caeb-11e9-ade6-0200cd936042";
	private static String uriBase = "";
    public static void main(String args[]) {
    	Scanner sc= new Scanner(System.in);
    	OtpSendFunctionality otpsend= new OtpSendFunctionality();
    	session_id=otpsend.send();
    	System.out.println("session_id"+session_id);
    	System.out.println("Enter otp");
    	otp_entered_by_user= sc.next();
		HttpClient httpclient = HttpClientBuilder.create().build();
		String faceId = "";
		try {
			uriBase = "https://2factor.in/API/V1/"+api_key+"/SMS/VERIFY/"+session_id+"/"+otp_entered_by_user;
			URIBuilder builder = new URIBuilder(uriBase);
            URI uri = builder.build();
			HttpPost request = new HttpPost(uri);
			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// Format and display the JSON response.
				String jsonString = EntityUtils.toString(entity);
				System.out.println("response"+jsonString);

			}
			else
				System.out.println("response null");
			
		} catch (Exception e) {
			// Display error message.
			System.out.println("message"+e.getMessage());
		}
	
    }
}

