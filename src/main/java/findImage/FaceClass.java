package findImage;

import java.io.File;
import java.net.URI;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

public class FaceClass {
	// Replace <Subscription Key> with your valid subscription key.
	private final String subscriptionKey = "89de252a509e4b2696b542c70be3869e";
	// NOTE: You must use the same region in your REST call as you used to
	// obtain your subscription keys. For example, if you obtained your
	// subscription keys from westus, replace "westcentralus" in the URL
	// below with "westus".
	//
	// Free trial subscription keys are generated in the "westus" region. If you
	// use a free trial subscription key, you shouldn't need to change this region.
	private final String uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect";

//	private  final String imageWithFaces = "{\"url\":\"https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg\"}";

	private final String faceAttributes = "age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

	public String detectFace() {
		// TODO Auto-generated method stub
		HttpClient httpclient = HttpClientBuilder.create().build();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Path");
		String faceId = "";
		String path = scan.next();
		path = path.replace("\\", "/");
		try {

			URIBuilder builder = new URIBuilder(uriBase);

			// Request parameters. All of them are optional.
			builder.setParameter("returnFaceId", "true");
			builder.setParameter("returnFaceLandmarks", "false");
			builder.setParameter("returnFaceAttributes", faceAttributes);

			// Prepare the URI for the REST API call.
			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);

			// Request headers. Replace the example key below with your valid subscription
			// key.
			request.setHeader("Content-Type", "application/octet-stream");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			// Request body

			File file = new File(path);

			FileEntity reqEntity = new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM);
			request.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();
			System.out.println(response.getStatusLine());

			if (entity != null) {
				String jsonString = EntityUtils.toString(entity).trim();
				JSONArray jsonArray = new JSONArray(jsonString);
				faceId = (String) jsonArray.getJSONObject(0).get("faceId");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return faceId;
	}

}
