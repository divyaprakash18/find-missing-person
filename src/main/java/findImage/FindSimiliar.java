package findImage;

//// This sample uses the Apache HTTP client from HTTP Components (http://hc.apache.org/httpcomponents-client-ga/)
import java.net.URI;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class FindSimiliar {
	private final String subscriptionKey = "89de252a509e4b2696b542c70be3869e";

	public void find(String faceListId) {
		String persistedFaceIdList[];
		double confidenceList[];
		HttpClient httpclient = HttpClients.createDefault();
		Scanner scan =  new Scanner(System.in);
		try {
			FaceClass face = new FaceClass();
			String faceId = face.detectFace();
			URIBuilder builder = new URIBuilder(
					"https://westcentralus.api.cognitive.microsoft.com/face/v1.0/findsimilars");

			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);
			request.setHeader("Content-Type", "application/json");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			// Request body
			JSONObject jsonBody = new JSONObject();
			jsonBody.put("faceId", faceId);
			jsonBody.put("faceListId",faceListId);
			jsonBody.put("maxNumOfCandidatesReturned", 10);
			jsonBody.put("mode", "matchFace");
			StringEntity reqEntity = new StringEntity(jsonBody.toString());
			request.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				String jsonString = EntityUtils.toString(entity).trim();
				JSONArray jsonArray = new JSONArray(jsonString);
				persistedFaceIdList = new String[jsonArray.length()];
				confidenceList = new double[jsonArray.length()];
				for(int i=0;i<jsonArray.length();i++) {
					persistedFaceIdList[i] = (String) jsonArray.getJSONObject(i).get("persistedFaceId");
					confidenceList[i] = (double) jsonArray.getJSONObject(i).get("confidence");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
