package genFaceList;

import java.net.URI;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class GenPersistenListId {
	private final String subscriptionKey = "89de252a509e4b2696b542c70be3869e";
	public String genPerId() {
		GenFaceListId genFace = new GenFaceListId();
		//First Generate FaceListID Then Generate Persist ID
		String faceListId = genFace.genFaceListId();
		HttpClient httpclient = HttpClients.createDefault();
		String persistId="";
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter url of the image");
		String url = scanner.nextLine();
		try {
			URIBuilder builder = new URIBuilder("https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/"
					+faceListId+ "/persistedFaces");

			URI uri = builder.build();
			HttpPost request = new HttpPost(uri);
			request.setHeader("Content-Type", "application/json");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			// Request body
			JSONObject jsonBody = new JSONObject();
			jsonBody.put("url", url);
			StringEntity reqEntity = new StringEntity(jsonBody.toString());
			request.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				String jsonString = EntityUtils.toString(entity).trim();
				JSONObject jsonObject = new JSONObject(jsonString);
				persistId = jsonObject.getString("persistedFaceId");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return persistId;
	}
	
}
