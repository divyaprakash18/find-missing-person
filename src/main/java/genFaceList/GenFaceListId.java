package genFaceList;

import java.net.URI;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.findmissingperson.model.FaceListID;

public class GenFaceListId {
	
	private final String subscriptionKey = "89de252a509e4b2696b542c70be3869e";
	FaceListID faceListID = new FaceListID();
//	private final String uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/face-list";
	
	public boolean getFaceListId() {
		HttpClient httpclient = HttpClients.createDefault();
        try
        {
            URIBuilder builder = new URIBuilder("https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists");
            builder.setParameter("returnRecognitionModel", "false");
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();
            
            
            if (entity != null) 
            {
            	JSONArray jsonArray = new JSONArray(EntityUtils.toString(entity).trim());
            	String faceListResponse = jsonArray.toString();
            	if(!faceListResponse.equals("[]")) {
    				faceListID.setFaceListID((String) jsonArray.getJSONObject(0).get("faceListId"));
    				return true;
            	}	
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
		
		return false;
	}
	
	public String genFaceListId() {
		boolean doesFaceListIdExists = getFaceListId();
		
		String faceListId;
		if(doesFaceListIdExists) {
			faceListId = faceListID.getFaceListID();
			return faceListId;
		}
		//FaceList POJO Class
		
		HttpClient httpclient = HttpClients.createDefault();
		Scanner scan = new Scanner(System.in);
		//faceList variable
		faceListID.setFaceListID("facelist");
		faceListId = faceListID.getFaceListID();
		try {
			URIBuilder builder = new URIBuilder(
					"https://westcentralus.api.cognitive.microsoft.com/face/v1.0/facelists/" + faceListId);

			URI uri = builder.build();
			HttpPut request = new HttpPut(uri);
			request.setHeader("Content-Type", "application/json");
			request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

			JSONObject jsonBody = new JSONObject();
			jsonBody.put("name", faceListId);

			// Request body
			StringEntity reqEntity = new StringEntity(jsonBody.toString());
			request.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(request);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				System.out.println(EntityUtils.toString(entity));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return faceListId;
	}
}
