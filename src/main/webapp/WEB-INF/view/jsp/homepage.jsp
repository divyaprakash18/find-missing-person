<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>

<head>
<meta charset="ISO-8859-1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TALAASH</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" href="resources/animate.css">
    <!-- Theme CSS -->
    <link href="resources/agency.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" href="resources/engine1/style.css" media="screen" />
	<style type="text/css">a#vlb{display:none}</style>
	<script type="text/javascript" src="resources/engine1/jquery.js"></script>
	
	<style>
	
	.ws_images .content {
  position: absolute; 
  bottom: 250px; 
  background: rgb(0, 0, 0); 
  background: rgba(0, 0, 0, 0.5); 
  color: #f1f1f1; 
  width: 100%; 
  padding: 10px; 
}

.dropdown {
  padding: 13px;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: black;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}


</style>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
				
                <a href="#page-top"><img src = "resources/m25.jpg" height = "100 px" width = "100 px"></a>
				
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
					 <li>
					  <div class="dropdown">
                        <a class="dropbtn" href="#page-top"><i class="fa fa-diamond" aria-hidden="true"></i>
 REGISTER</a> <div class="dropdown-content">
      <a href="showContactPersonForm">LOST SOMEONE??</a>
      <a href="registerUser">FOUND SOMEONE??</a>
     
    </div>
	</div>
                    </li>
					 <li>
					  <div class="dropdown">
                        <a class="dropbtn" href="#page-top"><i class="fa fa-diamond" aria-hidden="true"></i>
 LOGIN </a> <div class="dropdown-content">
      <a href="lostSomeoneLogin">LOST SOMEONE??</a>
      <a href="foundSomeoneLogin">FOUND SOMEONE??</a>
     
    </div>
	</div>
                    </li>
					 <li>
					  <div class="dropdown">
                        <a class="dropbtn" href="#page-top"><i class="fa fa-diamond" aria-hidden="true"></i>
 SEARCH </a> <div class="dropdown-content">
      <a href="listPerson">KNOWN</a>
      <a href="#">UNIDENTIFIED</a>
     
    </div>
	</div>
                    </li>
                    <li>
                        <a class="page-scroll" href="#problem"><i class="fa fa-diamond" aria-hidden="true"></i>
 PROBLEMS</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#idea"><i class="fa fa-diamond" aria-hidden="true"></i>
 OUR IDEA</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#gallery"><i class="fa fa-diamond" aria-hidden="true"></i>
 GALLERY</a>
                    </li>
                    <li>
                       
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact"><i class="fa fa-diamond" aria-hidden="true"></i>
 FEEDBACK</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!-- Start WOWSlider.com BODY section id=wowslider-container1 -->
	<div id="wowslider-container1">
	<div class="ws_images">
<a href="#"><img src="resources/data1/images/m1.jpg" alt="m1" title="Let's help people find way back home" id="wows0"/></a>
<div class="content">
    <h1 align="center">TALAASH- FIND YOUR DEAR ONES</h1>
   
  </div>
<a href="#"><img src="resources/data1/images/m2.jpg" alt="m2" title="Let's help people find way back home" id="wows1"/></a>
<div class="content">
    <h1 align="center">TALAASH- FIND YOUR DEAR ONES</h1>
   
  </div>
<a href="#"><img src="resources/data1/images/m3.jpg" alt="m3" title="Let's help people find way back home" id="wows2"/></a>
<div class="content">
    <h1 align="center">TALAASH- FIND YOUR DEAR ONES</h1>
   
  </div>
<a href="#"><img src="resources//images/m5.jpg" alt="m5" title="Let's help people find way back home" id="wows3"/></a>
<div class="content">
    <h1 align="center">TALAASH- FIND YOUR DEAR ONES</h1>
   
  </div>
</div>
<div class="ws_bullets"><div>
<a href="#wows0" title="m1"><img src="resources/data1/tooltips/m1.jpg" alt="m1"/>1</a>
<a href="#wows1" title="m2"><img src="resources/data1/tooltips/m2.jpg" alt="m2"/>2</a>
<a href="#wows2" title="m3"><img src="resources/data1/tooltips/m3.jpg" alt="m3"/>3</a>
<a href="#wows3" title="m5"><img src="resources/data1/tooltips/m5.jpg" alt="m5"/>4</a>
</div></div>
<a style="display:none" href="http://wowslider.com">jQuery 3d Image Carousel by WOWSlider.com v1.7</a>
	<div id="wowslider-frame"></div>
	<div id="wowslider-shadow"></div>
	</div>
	<script type="text/javascript" src="resources/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
   

    <!-- problem Section -->
    <section id="problem" data-parallax="scroll" data-image-src="resources/m23.jpg" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><i class="fa fa-diamond" aria-hidden="true"></i> THE PROBLEM</h2>
                    <h3 class="section-subheading text-muted">There are many people found missing everyday.</h3>
                </div>
            </div>
            <div class="row text-center">
             <p align="justify" >  Every day thousands of people are reported missing in our country and many of them are never found. The cases of "Missing people" can be broadly categorized into 
people who are missing, runaway, abducted/kidnapped, deserted, untraced, escaped, unidentified dead body or an unidentified person etc… Despite the efforts 
of the police and other agencies countless children, women and men are missing and many of them land in the hands of exploiters and evil /hostile environments. 
"Despite being the national repository of 'crime data', the NCRB is unaware both of children who are traced or of those who remain untraced".
This website seeks to address these gaps so as to ensure that all the missing women, children and men are recorded & restored and also ensure that 
perpetrators who exploited them are exposed and punished. With its multiple complexities the issue of "missing people” it is impossible for one person/s, 
agency or government department to address the issue of missing persons. Adding to this complexity is the lack of proper data and monitoring/tracking of "missing 
persons". It is time the civil society comes together to address this issue.The website seeks to complement the services of the Police /CID Department who have 
assured unstinted efforts to help identify the missing person brought to their notice through this website. The website also gives you details of what you need to do,
once you notice that your family member/friend/or person you know is "missing" or as a concerned citizen/organization you find a person who has lost his moorings and
is in need of help.</p>
            </div>
        </div>
    </section>

    <!-- idea Grid Section -->
    <section id="idea" data-parallax="scroll" data-image-src="resources/m8.jpg">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">OUR IDEA</h2>
                    <h3 class="section-subheading text-muted">Lets help people find way back home</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                   <p align = "justify">It is a web application, which allows you to share the picture and details of people you see on road side, and feel they are lost. 
Kids and old people who are forced into begging or kidnapped and being used for several illegal activities can find their way back home. 
Files of FIRs are filled with such cases, however best possible way to get them back to their home is using the technologies to make them meet their families. 
Even if we can do it for one of them, our purpose is solved.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- gallery Section -->
    <section id="gallery" class="bg-light-gray">
        <div class="container">
          
<div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><i class="fa fa-bell" aria-hidden="true"></i> GALLERY</h2>
                    <h3 class="section-subheading text-muted">Happy to help these people meet there family</h3>
                </div>
            </div>
            <div class="row">
               <table>
			   <tr>
			   <td><img src="resources/m41.jpg" class="img-responsive" alt=""></td>
			   <td><img src="resources/m42.jpg" class="img-responsive" alt=""></td>
			   <td><img src="resources/m43.jpg" class="img-responsive" alt=""></td>
			   </tr>
			     <tr>
			   <td><img src="resources/m44.jpg" class="img-responsive" alt=""></td>
			   <td><img src="resources/m45.jpg" class="img-responsive" alt=""></td>
			   <td><img src="resources/m46.jpg" class="img-responsive" alt=""></td>
			   </tr>
			   
</table>			   
            </div> 
        </div>
    </section>

   
    <!-- Clients Aside -->
    <aside class="clients" data-parallax="scroll" data-image-src="img/header-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="text-center">Make a difference in life of thousands of missing people </h1>
                </div>
            </div>
        </div>
    </aside>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">SHARE YOUR FEEDBACK</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">No Copyright, Free for commercial use</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Boom Boom</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

   

    <!-- jQuery -->
    <script src="resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="resources/jqBootstrapValidation.js"></script>
    <script src="resources/contact_me.js"></script>
    
<!--    Parallex scripts-->
    <script src="resources/parallax.js"></script>

    <!-- Theme JavaScript -->
    <script src="resources/agency.min.js"></script>

</body>

</html>
