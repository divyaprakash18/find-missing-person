<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="resources/validate.js"></script>
<title>Report Lost Person</title>
</head>
<body>
	<c:url var="addAction" value="/addMissing"></c:url>
	<form:form action="${addAction}" name="reportMissing"
		modelAttribute="lostPerson" method="POST">

		<table>
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="fName" path="fName"></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="lName" path="lName"></td>
			</tr>

			<tr>
				<td>Age:</td>
				<td><input type="text" name="age" path="age"></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td><select name="gender" path="gender">
						<option>Male</option>
						<option>Female</option>
						<option>Other</option>
				</select></td>
			</tr>
			<tr>
				<td>Address Line 1:</td>
				<td><input type="text" name="address1" path="address1"></td>
			</tr>
			<tr>
				<td>Address Line 2:</td>
				<td><input type="text" name="address2" path="address2"></td>
			</tr>
			<tr>
				<td>Pincode :</td>
				<td><input type="numeric" name="pincode" path="pincode"></td>
			</tr>
			<tr>
				<td>Height:</td>
				<td><input type="text" name="height" path="height"></td>
			</tr>
			<tr>
				<td>Weight:</td>
				<td><input type="text" name="weight" path="weight"></td>
			</tr>
			<tr>
				<td>Hair Colour:</td>
				<td><select name="hairColour" path="hairColour">
						<option>Black</option>
						<option>Brown</option>
						<option>Grey</option>
				</select></td>
			</tr>
			<tr>
				<td>Eye Colour:</td>
				<td><select name="eyeColour" path="eyeColour">
						<option>Black</option>
						<option>Brown</option>
						<option>Blue</option>
						<option>Green</option>
						<option>Grey</option>
				</select></td>
			</tr>
			<tr>
				<td>Identification Mark:</td>
				<td><input type="text" name="identificationMark"
					path="identificationMark"></td>
			</tr>
			<tr>
				<td>Upload Photo:</td>
				<td><input type="file" name="imgPath" path="imgPath" accept="image/jpeg"></td>
			</tr>

		</table>
		<input type="submit" value="Add Missing" />
	</form:form>
	<!--  /submitMissing  -- success page -->
</body>
</html>