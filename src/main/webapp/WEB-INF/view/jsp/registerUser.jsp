<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- <script src="resources/validate.js"></script> -->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%-- <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/styles.css" /> --%>

<title>Register New User</title>
</head>
<body>

	<!-- 
=======
register page
Registration form
/sendOTP
/addUser  (go to login)
-->
	<%-- <c:url var="addAction" value="/addUser"></c:url> --%>

	<form:form action="addHelper" modelAttribute="user" name="addHelperForm"
		method="POST">
		<table>
		
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="fName" path="fName"></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="lName" path="lName"></td>
			</tr>
			<tr>
				<td>Phone Number:</td>
				<td><input type="text" name="phone" path="phone"></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="email" path="email"></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" path="password"></td>
			</tr>
			<tr>
				<td>Confirm Password:</td>
				<td><input type="password" name="cpassword"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Register" />
			</tr>
		</table>
	</form:form>
</body>
</html>